//
//  ChildViewCell.swift
//  PracticalTechStub
//
//  Created by Bharat Lalwani on 26/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class ChildViewCell: UITableViewCell {

    @IBOutlet weak var lblChildHeader: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    var indexCount = 1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnSubstractAction(_ sender: Any) {
        indexCount = indexCount - 1
        lblCount.text = "\(indexCount)"
    }
    @IBAction func btnAddAction(_ sender: Any) {
        indexCount = indexCount + 1
        lblCount.text = "\(indexCount)"
    }
}
