//
//  HeaderViewCell.swift
//  PracticalTechStub
//
//  Created by Bharat Lalwani on 26/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class HeaderViewCell: UITableViewCell/*, UITableViewDelegate, UITableViewDataSource */{

    @IBOutlet weak var heightChildTblView: NSLayoutConstraint!
    @IBOutlet weak var tblViewChildViews: UITableView!
//    var tblViewChildViews: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    var dataObject : Datamodel?
    var childAddCallback : (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
//        tblViewChildViews = UITableView(frame: .zero, style:UITableView.Style.plain)
        
//        tblViewChildViews.delegate = self
//        tblViewChildViews.dataSource = self
//        tblViewChildViews.estimatedRowHeight = UITableView.automaticDimension
//        self.layoutIfNeeded()
//        heightChildTblView.constant = self.tblViewChildViews.contentSize.height
//       self.addSubview(tblViewChildViews!)
        // Initialization code
    }

//    override func layoutSubviews() {
//         super.layoutSubviews()
//         self.layoutIfNeeded()
////        heightChildTblView.constant = self.tblViewChildViews.contentSize.height
////        tblViewChildViews?.frame = CGRect(x: 0.2, y: 0.3, width: self.bounds.size.width-5, height: self.bounds.size.height-5)
//     }
    
    func addChildViews() {
        let parentView = self.contentView.subviews[1]
        parentView.subviews.forEach({ $0.removeFromSuperview() })
        for object in (dataObject?.childObj)!{
            let childView = ChildView(frame: CGRect(origin: CGPoint(x: .zero, y: CGFloat((object.index ?? 0) * 42)), size: CGSize(width: self.contentView.frame.width, height: 42)))
            childView.lblHeader.text = "Child \(object.index ?? 0)"
            childView.lblIndex.text = "\(object.cellValue)"
            childView.childObj = object
            childView.tag = Int((dataObject?.index ?? 0) * 100) + Int(object.index ?? 1)
            parentView.addSubview(childView)
        }
//        self.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataObject?.childObj?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "ChildViewCell", for: indexPath) as! ChildViewCell
        cell.lblChildHeader.text = "Child \(dataObject?.childObj?[indexPath.row].index ?? 0)"
        cell.lblCount.text = "\(dataObject?.childObj?[indexPath.row].cellValue ?? 0)"
        return cell
     }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func addChildAction(_ sender: Any) {
        let childObj = Child(index: dataObject?.childObj?.count ?? 0)
        dataObject?.childObj?.append(childObj)
//        self.tblViewChildViews.reloadData()
        if let childCallback = self.childAddCallback{
            childCallback()
        }
//        self.tblViewChildViews.reloadData()
        self.layoutIfNeeded()
    }
}
