//
//  ChildView.swift
//  PracticalTechStub
//
//  Created by Bharat Lalwani on 26/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class ChildView: UIView {
    
    var view: UIView!

    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    var childObj : Child?
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let nibName = "ChildView"
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    @IBAction func btnSubstractAction(_ sender: Any) {
        if childObj?.cellValue == 0 {return}
        childObj?.cellValue = (childObj?.cellValue ?? 1) - 1
        lblIndex.text = "\(childObj?.cellValue ?? 1)"
    }
    @IBAction func btnAddAction(_ sender: Any) {
        childObj?.cellValue = (childObj?.cellValue ?? 0) + 1
        lblIndex.text = "\(childObj?.cellValue ?? 0)"
    }
}
