//
//  ViewController.swift
//  PracticalTechStub
//
//  Created by Bharat Lalwani on 26/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var datamodel : [Datamodel] = []
    @IBOutlet weak var tblHeaderListView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblHeaderListView.delegate = self
        tblHeaderListView.dataSource = self
//        tblHeaderListView.register(UINib(nibName:"ChildView", bundle: nil), forCellReuseIdentifier: "HeaderViewCell")
        
        // Do any additional setup after loading the view.
        //    tblHeaderListView.estimatedRowHeight = 44.0
//        tblHeaderListView.rowHeight = UITableView.automaticDimension
    }
    
    //Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datamodel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewCell", for: indexPath) as! HeaderViewCell
        let dataObject = datamodel[indexPath.row]
        dataObject.index = indexPath.row
        cell.dataObject = dataObject
        cell.lblHeader.text = "Header \(indexPath.row)"
        cell.childAddCallback = {
            self.tblHeaderListView.reloadRows(at: [indexPath], with: .fade)
        }
        cell.addChildViews()
        return cell
    }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return (CGFloat(62 + ((datamodel[indexPath.row].childObj?.count ?? 0)*42)))
        }
    
    @IBAction func addHeaderAction(_ sender: Any) {
        let dataObject = Datamodel(index: self.datamodel.count)
        self.datamodel.append(dataObject)
        self.tblHeaderListView.reloadData()
    }
    
}

