//
//  DataModel.swift
//  PracticalTechStub
//
//  Created by Bharat Lalwani on 26/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import Foundation
class Datamodel {
    var index: Int?
    init(index: Int) {
        self.index = index
    }
    var childObj : [Child]? = []
}

class Child {
    var index: Int?
    var cellValue : Int = 0
    init(index: Int) {
        self.index = index
    }
    
}

